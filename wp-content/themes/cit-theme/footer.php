<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

$container = get_theme_mod( 'understrap_container_type' );
?>

<?php //get_template_part( 'sidebar-templates/sidebar', 'footerfull' ); ?>

<div class="wrapper" id="wrapper-footer">

	<div class="container-fluid">
			<div class="row">

				<div class="col-md-12">

					<footer class="site-footer" id="colophon">

						<div class="site-info">
							<div class="container-1400">
							<div class="row main-footer">
								<div class="col-md-3 footer-single-col">
									<div class="logo">
										<?php $logo = get_field('footer_logo','options') ?>
										<img src="<?php echo $logo['url']; ?>" alt="">
									</div>
								</div>
								<div class="col-md-3 footer-single-col middle-col">
									<div class="menu-middle-col">
								<?php
								$menu_obj = monoprog_get_menu_by_location('footer-menu-first');
								echo "<h4>".esc_html($menu_obj->name)."</h4>"; 
								?>
								<?php wp_nav_menu( array( 'theme_location' => 'footer-menu-first' ) ); ?>
								
									</div>
								</div>
								<div class="col-md-3 footer-single-col middle-col">
									<div class="menu-middle-col">
										<?php
								$menu_obj = monoprog_get_menu_by_location('footer-menu-second');
								echo "<h4>".esc_html($menu_obj->name)."</h4>"; 
								?>
									<?php wp_nav_menu( array( 'theme_location' => 'footer-menu-second' ) ); ?>
									</div>
								</div>
								<div class="col-md-3 footer-single-col">
									<h4>Contact Us</h4>
									<div class="contacts">
									<a href="mailto:<?php the_field('footer_email','options');?>"><i class="fa fa-envelope"></i>Email: <?php the_field('footer_email','options');?> </a>
									<a href="tel:+355<?php the_field('footer_telephone','options');?>"><i class="fa fa-phone"></i>Telephone: <?php the_field('footer_telephone','options');?></a>
									<a href=""><i class="fa fa-map-marker"></i>Address: <?php the_field('footer_address','options');?></a>
									</div>
								</div>
							</div>
								<div class="bottom-footer">
									<div class="row">
										<div class="col-md-3"><h4 class="slogan-text"><?php the_field('slogan_text','options') ?></h4></div>
										<div class="offset-md-6 col-md-3"><p class="follow-us"><?php the_field('follow_us','options') ?>
											<?php while(have_rows('social_links','options')): the_row(); ?>
												<a href="<?php the_sub_field('social_link_url','options') ?>"><i class="fa fa-<?php the_sub_field('social_link_title','options') ?>"></i></a>
											<?php endwhile; ?>
										</p>
										</div>
									</div>
								</div>
								</div>

						</div><!-- .site-info -->

					</footer><!-- #colophon -->

				</div><!--col end -->

			</div><!-- row end -->

			<div class="to-top">
				<a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>
			</div>

			<div class="side-strip">
				
				<ul class="side-strip-list">

					<!-- Search Button -->
					<li class="search">
						<a href="#" ><i class="fa fa-search"></i></a>
					</li>
					<div class="search-area">
										<?php get_search_form(); ?>
						</div>

					<!-- Map Button -->
					<li class="map">
						<a href="#"><i class="fa fa-map-marker "></i></a>
					</li>
					<div class="map-area">
						<?php $image = get_field('map_image','options'); ?>
						<?php $marker = get_field('map_marker','options'); ?>
						<a href="<?php the_field('cta_link','options'); ?>">
							<img src="<?php echo $image['url']; ?>" alt="">
							<img class="map-marker" src="<?php echo $marker['url']; ?>" alt="">
						</a>
						<div class="cta-link">
						<a href="<?php the_field('cta_link','options'); ?>" class=""><?php the_field('cta_text','options') ?></a>
						</div>
					</div>

					<!-- Contact Button -->
					<li class="mail">
						<a href="#" ><i class="fa fa-envelope "></i></a>
					</li>
					<div class="contact-form">
						<?php $formId = get_field('contact_form_id','options'); ?>
						<?php $formTitle = get_field('contact_form_title','options'); ?>
						<?php echo do_shortcode( '[contact-form-7 id="'.$formId.'" title="'.$formTitle.'"]' ); ?>
					</div>
				
				</ul>
			</div>

	</div><!-- container end -->

</div><!-- wrapper end -->

</div><!-- #page we need this extra closing tag here -->



<?php wp_footer(); ?>

</body>

</html>

