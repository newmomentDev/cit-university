<?php

get_header(); ?>
<div class="container-fluid">
	<section class="section">
		<div class="row categories-row">
		<?php wp_nav_menu( array( 'theme_location' => 'faculties-menu' ) ); ?>
		</div>
	</section>
	<div class="container-1600">
	<section class="section section-bachelors">
		<div class="row">
			<div class="col-12">
				<div class="row">
					<div class="col-12 title">
						<h1 class="red-title"> <?php the_field('bachelor_title','options') ?> </h1>
					</div>
				</div>
			</div>
			<?php
			global $wp;
			$cat = explode("/", $wp->request);
			
			$sub_cat = explode("-", $cat[1]);

			$args = array(
			'post_type' => 'program',
			'posts_per_page' => -1,
			'tax_query' => array(
				'relation' => 'AND',
				array(
					'taxonomy' => 'faculties',
					'field'    => 'slug',
					'terms'    => $cat[1],
					),
				array(
					'taxonomy' => 'faculties',
					'field'    => 'slug',
					'terms'    => 'bachelor-'.$sub_cat[2],
					),
				)
			);
			$query = new WP_Query( $args ); ?>

			<?php 
			$term = get_queried_object();
			$video = get_field('video',$term); ?>
			<div class="container-1600">
				<div class="row">			 
				<?php 
				// Check that we have query results.
				if ( $query->have_posts() ) {
				    // Start looping over the query results.
				    while ( $query->have_posts() ) { 
				    	$query->the_post();?> 	 
				    	
				    	<div class="col-md-6 col-lg-4 program-col">
				    		<a href="<?php echo get_permalink (); ?>">
					    		<div class="single-program" style="background-image: url('<?php echo get_the_post_thumbnail_url(); ?>');">
					    			<div class="data">
					    				<h3><?php echo(get_the_title()); ?></h3>
					    			</div>
					    		</div>
				    		</a>
				    	</div>
				     
				    <?php }
				 
				}
				 
				// Restore original p
				wp_reset_postdata();
				?>
				</div>
			</div>

		</div>
	</section>
	<section class="section section-masters">
		<div class="row">
			<div class="col-12">
				<div class="row">
					<div class="col-12 title">
						<h1 class="red-title"> <?php the_field('master_title','options') ?></h1>
					</div>
					<?php
			$args = array(
			'post_type' => 'program',
			'posts_per_page' => -1,
			'tax_query' => array(
				'relation' => 'AND',
				array(
					'taxonomy' => 'faculties',
					'field'    => 'slug',
					'terms'    => $cat[1],
					),
				array(
					'taxonomy' => 'faculties',
					'field'    => 'slug',
					'terms'    => 'master-'.$sub_cat[2],
					),
				)
			);
			$query = new WP_Query( $args ); ?>
			<div class="container-1600">
				<div class="row">			 
				<?php 
				// Check that we have query results.
				if ( $query->have_posts() ) {
				    // Start looping over the query results.
				    while ( $query->have_posts() ) { 
				    	$query->the_post();?> 	 
				    	
				    	<div class="col-md-6 col-lg-4 program-col">
				    		<a href="<?php echo get_permalink (); ?>">
					    		<div class="single-program" style="background-image: url('<?php echo get_the_post_thumbnail_url(); ?>');">
					    			<div class="data">
					    				<h3><?php echo(get_the_title()); ?></h3>
					    			</div>
					    		</div>
				    		</a>
				    	</div>
				     
				    <?php }
				 
				}
				 
				// Restore original p
				wp_reset_postdata();
				?>
				</div>
			</div>

				</div>
			</div>
		</div>
	</section>
	</div>

					<div class="video-section">
						<span class="close">
							<i class="fa fa-times"></i>
						</span>
				<video autoplay controls id="video-tax">
					<source src="<?php echo $video['url']; ?>" type="video/mp4">
				</video>
			</div>

	
</div>
<?php get_footer();