<?php
/**
 * The template for displaying 404 pages (not found).
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

get_header();

$container = get_theme_mod( 'understrap_container_type' );
?>

<div class="wrapper" id="error-404-wrapper">

	<div class="container-1600" id="content" tabindex="-1">

		<div class="row">

			<div class="col-md-12 content-area" id="primary">
					
					<div class="404-area-text">
						<h1 class="black-title">Ooops!</h1>
						<h1 class="red-error">404</h1>
						<h1 class="black-title text-title">Page not found</h1>
						<a href="#" class="cta-link">Go back</a>
						<div class="end-text">
							<p><p class="black">Error</p>the requested page does not exist</p>
						</div>
					</div>
					

				</main><!-- #main -->

			</div><!-- #primary -->

		</div><!-- .row -->

	</div><!-- #content -->

</div><!-- #error-404-wrapper -->

<?php get_footer(); ?>
