<?php get_header(); ?>
<div class="container-1200">
	<div class="row">
		<div class="col-12">
			<div class="page-title d-flex justify-content-center">
				<h1 class="red-title"><?php the_title(); ?></h1>
			</div>
		</div>
		<div class="col-12">
			<div class="row">
				<div class="col-lg-2">
				<?php if(have_rows('tabs')): ?>
	<ul class="nav nav-tabs flex-column" role="tablist">
	<?php $count = 1; ?>
	<?php while(have_rows('tabs')) : the_row(); ?>
		<?php $title = get_sub_field('tab_title'); ?>
		<?php $titleId = clean(strtolower(str_replace(" ","-",$title))); ?>
		<li class="nav-item">
			<a class="nav-link<?php if($count == 1){echo ' active';} ?>" id="<?php echo $titleId; ?>-tab" data-toggle="tab" href="#<?php echo $titleId; ?>" role="tab" aria-controls="<?php echo $titleId; ?>" aria-selected="false">
				<?php the_sub_field('tab_title'); ?>	
			</a>
		</li>
	<?php $count++; ?>				
	<?php endwhile; ?>
	</ul>
</div>
<div class="col-lg-10">
	<div class="tab-content">
				<?php $active = 1; ?>
				<?php while(have_rows('tabs')) : the_row(); ?>
					<?php $title = get_sub_field('tab_title'); ?>
				<?php $titleId = clean(strtolower(str_replace(" ","-",$title))); ?>
				<div class="tab-pane fade<?php if($active == 1){echo ' active show';} ?>" id="<?php echo $titleId; ?>" role="tabpanel" aria-labelledby="<?php echo $titleId; ?>-tab">
				<?php the_sub_field('editor') ?>			
				</div>
					

				<?php $active++; ?>				
				<?php endwhile; ?>
			</div>
<?php endif; ?>
</div>
</div>
		</div>
	</div>
</div>
<?php get_footer();