<div class="col-12 col-md-4 col-lg-3 col-xl-2">
<div class="persona-lecturer">
			<img src="<?php the_post_thumbnail_url() ?>" class="img-resonsive" alt="">
			<div class="data">
				<h2 class="title"><?php the_title(); ?></h2>
				<?php $position = get_field_object('position') ?>
				<p class="position-title"><?php echo $position['value'] ?></p>
				<p class="position-description"><?php the_field('position_description') ?></p>
				<div class="contact">
					<?php while(have_rows('contact_data')): the_row(); ?>
						<?php if(get_sub_field('phone_number') != null): ?>
						<!-- Phone Number -->
						<?php $phone = get_sub_field_object('phone_number'); ?>
						<a href="tel:<?php the_sub_field('phone_number') ?>"><i class="fa fa-phone"></i><?php the_sub_field('phone_number')?></a>
						<?php endif; ?>
						<?php if(get_sub_field('email_address') != null): ?>
						<!-- Email Address -->
						<?php $email = get_sub_field_object('email_address'); ?>
						<a href="mailto:<?php the_sub_field('email_address') ?>"><i class="fa fa-envelope"></i><?php the_sub_field('email_address')?></a>
						<?php endif; ?>
					<?php endwhile; ?>
				</div>
				<div class="social-links">
					<?php while(have_rows('social_links')): the_row(); ?>
						<a href="<?php the_sub_field('link') ?>"><i class="fa fa-<?php the_sub_field('icon'); ?>"></i></a>
					<?php endwhile; ?>
				</div>
			</div>
</div>
</div>