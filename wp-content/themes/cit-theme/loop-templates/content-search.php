<?php
/**
 * Search results partial template.
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}
?>

<article <?php post_class(); ?> id="post-<?php the_ID(); ?>">

	<header class="entry-header">

		<?php
		the_title(
			sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ),
			'</a></h2>'
		);
		?>

	</header><!-- .entry-header -->

	<div class="entry-summary">
		<p>
			<?php if(!empty(the_excerpt())): 
			the_excerpt(); ?> 
			<a href="<?php the_permalink() ?>"><b>Read more</b></a>
			<?php 
			else:
				the_field('description'); ?>
				<a href="<?php the_permalink() ?>"><b>Read more</b></a>
			 <?php endif; ?>
		</p>
	</div><!-- .entry-summary -->

</article><!-- #post-## -->
