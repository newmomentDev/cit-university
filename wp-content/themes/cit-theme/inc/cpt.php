<?php


$labels = array(
    'name'                  =>   __( 'Events', 'uep' ),
    'singular_name'         =>   __( 'Event', 'uep' ),
    'add_new_item'          =>   __( 'Add New Event', 'uep' ),
    'all_items'             =>   __( 'All Events', 'uep' ),
    'edit_item'             =>   __( 'Edit Event', 'uep' ),
    'new_item'              =>   __( 'New Event', 'uep' ),
    'view_item'             =>   __( 'View Event', 'uep' ),
    'not_found'             =>   __( 'No Events Found', 'uep' ),
    'not_found_in_trash'    =>   __( 'No Events Found in Trash', 'uep' )
);
 
$supports = array(
    'title',
    'editor',
    'excerpt',
    'thumbnail'
);
 $args = array(
    'label'         =>   __( 'Events', 'uep' ),
    'labels'        =>   $labels,
    'description'   =>   __( 'A list of upcoming events', 'uep' ),
    'public'        =>   true,
    'show_in_menu'  =>   true,
    'menu_icon'     =>   'dashicons-calendar',
    'has_archive'   =>   true,
    'rewrite'       =>   true,
    'supports'      =>   $supports,
    'has_archive'   =>   'events'
);

 register_post_type( 'event', $args );


$labels = array(
    'name'                  =>   __( 'News', 'uep' ),
    'singular_name'         =>   __( 'News', 'uep' ),
    'add_new_item'          =>   __( 'Add New News', 'uep' ),
    'all_items'             =>   __( 'All News', 'uep' ),
    'edit_item'             =>   __( 'Edit News', 'uep' ),
    'new_item'              =>   __( 'New News', 'uep' ),
    'view_item'             =>   __( 'View News', 'uep' ),
    'not_found'             =>   __( 'No News Found', 'uep' ),
    'not_found_in_trash'    =>   __( 'No News Found in Trash', 'uep' )
);
 
$supports = array(
    'title',
    'editor',
    'thumbnail'
);
 $args = array(
    'label'         =>   __( 'News', 'uep' ),
    'labels'        =>   $labels,
    'description'   =>   __( 'A list of news', 'uep' ),
    'public'        =>   true,
    'show_in_menu'  =>   true,
    'menu_icon'     =>   'dashicons-calendar',
    'has_archive'   =>   true,
    'rewrite'       =>   true,
    'supports'      =>   $supports,
     'has_archive'   =>   'news'
);

 register_post_type( 'news', $args );


$labels = array(
    'name'                  =>   __( 'Testimonial', 'uep' ),
    'singular_name'         =>   __( 'Testimonial', 'uep' ),
    'add_new_item'          =>   __( 'Add New Testimonial', 'uep' ),
    'all_items'             =>   __( 'All Testimonials', 'uep' ),
    'edit_item'             =>   __( 'Edit Testimonial', 'uep' ),
    'new_item'              =>   __( 'New Testimonial', 'uep' ),
    'view_item'             =>   __( 'View Testimonial', 'uep' ),
    'not_found'             =>   __( 'No Testimonials Found', 'uep' ),
    'not_found_in_trash'    =>   __( 'No Testimonials Found in Trash', 'uep' )
);
 
$supports = array(
    'title'
);
 

$args = array(
    'label'         =>   __( 'Tesimonials', 'uep' ),
    'labels'        =>   $labels,
    'description'   =>   __( 'A list of all testimonials', 'uep' ),
    'exclude_from_search' => true,
    'public'        =>   true,
    'show_in_menu'  =>   true,
    'menu_icon'     =>   'dashicons-testimonial',
    'has_archive'   =>   true,
    'rewrite'       =>   true,
    'supports'      =>   $supports
);

 
register_post_type( 'testimonial', $args );



$labels = array(
    'name'                  =>   __( 'Programms', 'uep' ),
    'singular_name'         =>   __( 'Program', 'uep' ),
    'add_new_item'          =>   __( 'Add New Program', 'uep' ),
    'all_items'             =>   __( 'All Programms', 'uep' ),
    'edit_item'             =>   __( 'Edit Program', 'uep' ),
    'new_item'              =>   __( 'New Program', 'uep' ),
    'view_item'             =>   __( 'View Program', 'uep' ),
    'not_found'             =>   __( 'No Programms Found', 'uep' ),
    'not_found_in_trash'    =>   __( 'No Programms Found in Trash', 'uep' )
);
 
$supports = array(
    'title',
    'thumbnail'
);
 

$args = array(
    'label'         =>   __( 'Program', 'uep' ),
    'labels'        =>   $labels,
    'description'   =>   __( 'A list of all programms', 'uep' ),
    'public'        =>   true,
    'show_in_menu'  =>   true,
    'menu_icon'     =>   'dashicons-book-alt',
    'has_archive'   =>   true,
    'rewrite'       =>   true,
    'supports'      =>   $supports,
    'has_archive' 	=> 	 'faculty',
);
 
register_post_type( 'program', $args );

register_taxonomy( 'faculties', array('program'), array(
        'hierarchical' => true, 
        'label' => 'Faculties', 
        'singular_label' => 'Faculty', 
        'rewrite' => array( 'slug' => 'faculty', 'with_front'=> false )
        )
    );



$labels = array(
    'name'                  =>   __( 'Academic Staff', 'uep' ),
    'singular_name'         =>   __( 'Academic Staff', 'uep' ),
    'add_new_item'          =>   __( 'Add New Staff', 'uep' ),
    'all_items'             =>   __( 'All Staffs', 'uep' ),
    'edit_item'             =>   __( 'Edit Staff', 'uep' ),
    'new_item'              =>   __( 'New Staff', 'uep' ),
    'view_item'             =>   __( 'View Staff', 'uep' ),
    'not_found'             =>   __( 'No Staff Found', 'uep' ),
    'not_found_in_trash'    =>   __( 'No Staff Found in Trash', 'uep' )
);
 
$supports = array(
    'title',
    'thumbnail'
);
 

$args = array(
    'label'         =>   __( 'Academic Staff', 'uep' ),
    'labels'        =>   $labels,
    'description'   =>   __( 'A list of all Staff', 'uep' ),
    'public'        =>   true,
    'show_in_menu'  =>   true,
    'menu_icon'     =>   'dashicons-networking',
    'has_archive'   =>   true,
    'rewrite'       =>   true,
    'supports'      =>   $supports,
    'has_archive'   =>   false,
);
 
register_post_type( 'academic-staff', $args );

register_taxonomy( 'staff-faculties', array('academic-staff'), array(
        'hierarchical' => true, 
        'label' => 'Faculties', 
        'singular_label' => 'Faculty', 
        'rewrite' => array( 'slug' => 'staff-list', 'with_front'=> false )
        )
    );

$labels = array(
    'name'                  =>   __( 'Administration', 'uep' ),
    'singular_name'         =>   __( 'Administration', 'uep' ),
    'add_new_item'          =>   __( 'Add New Staff', 'uep' ),
    'all_items'             =>   __( 'Staff List', 'uep' ),
    'edit_item'             =>   __( 'Edit Staff', 'uep' ),
    'new_item'              =>   __( 'New Staff', 'uep' ),
    'view_item'             =>   __( 'View Staff', 'uep' ),
    'not_found'             =>   __( 'No Staff Found', 'uep' ),
    'not_found_in_trash'    =>   __( 'No Staff Found in Trash', 'uep' )
);
 
$supports = array(
    'title',
    'thumbnail'
);
 

$args = array(
    'label'         =>   __( 'Administration Staff', 'uep' ),
    'labels'        =>   $labels,
    'description'   =>   __( 'A list of all Staff', 'uep' ),
    'public'        =>   true,
    'show_in_menu'  =>   true,
    'menu_icon'     =>   'dashicons-networking',
    'has_archive'   =>   true,
    'rewrite'       =>   true,
    'supports'      =>   $supports,
    'has_archive'   =>   false,
);
 
register_post_type( 'administration-staff', $args );

$labels = array(
    'name'                  =>   __( 'Careers', 'uep' ),
    'singular_name'         =>   __( 'Careers', 'uep' ),
    'add_new_item'          =>   __( 'Add New Job Vacancy', 'uep' ),
    'all_items'             =>   __( 'Vacancy List', 'uep' ),
    'edit_item'             =>   __( 'Edit Vacancy', 'uep' ),
    'new_item'              =>   __( 'New Vacancy', 'uep' ),
    'view_item'             =>   __( 'View Vacancy', 'uep' ),
    'not_found'             =>   __( 'No Vacancy Found', 'uep' ),
    'not_found_in_trash'    =>   __( 'No Vacancy Found in Trash', 'uep' )
);
 
$supports = array(
    'title',
    'editor',
    'thumbnail'
    );
 

$args = array(
    'label'         =>   __( 'Careers', 'uep' ),
    'labels'        =>   $labels,
    'description'   =>   __( 'A list of all Job Vacanices', 'uep' ),
    'public'        =>   true,
    'show_in_menu'  =>   true,
    'menu_icon'     =>   'dashicons-businessman',
    'has_archive'   =>   true,
    'rewrite'       =>   true,
    'supports'      =>   $supports,
    'has_archive'   =>   false,
);

register_post_type( 'job-vacancy', $args );

register_taxonomy( 'categories', array('job-vacancy'), array(
        'hierarchical' => true, 
        'label' => 'Categories', 
        'singular_label' => 'Category', 
        'rewrite' => array( 'slug' => 'Category', 'with_front'=> false )
        )
    );



$labels = array(
    'name'                  =>   __( 'Conference', 'uep' ),
    'singular_name'         =>   __( 'Conference', 'uep' ),
    'add_new_item'          =>   __( 'Add New Conference', 'uep' ),
    'all_items'             =>   __( 'Conference List', 'uep' ),
    'edit_item'             =>   __( 'Edit Conference', 'uep' ),
    'new_item'              =>   __( 'New Conference', 'uep' ),
    'view_item'             =>   __( 'View Conference', 'uep' ),
    'not_found'             =>   __( 'No Conference Found', 'uep' ),
    'not_found_in_trash'    =>   __( 'No Conference Found in Trash', 'uep' )
);

$args = array(
    'label'         =>   __( 'Conferences', 'uep' ),
    'labels'        =>   $labels,
    'description'   =>   __( 'A list of all Conferences', 'uep' ),
    'public'        =>   true,
    'show_in_menu'  =>   true,
    'menu_icon'     =>   'dashicons-clipboard',
    'has_archive'   =>   true,
    'rewrite'       =>   true,
    'supports'      =>   $supports,
    'has_archive'   =>   false,
);

register_post_type( 'conference', $args );