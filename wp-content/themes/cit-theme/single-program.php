<?php 
get_header(); ?>

<div class="container-fluid">
	<section class="section">
		<div class="row categories-row">
		<?php wp_nav_menu( array( 'theme_location' => 'faculties-menu' ) ); ?>
		</div>
	</section>
	<div class="container-1600">
		<div class="row">
			<div class="col-12">
				<section class="section">
					<div class="row top-info-row">
						<div class="col-lg-6">
							<?php $image = get_field('program_image'); ?>
							<div class="image" style="background-image: url('<?php echo $image['url']; ?>');"></div>
						</div>
						<div class="col-lg-6">
							<div class="data">
								<h1 class="black"><?php the_field('program_title') ?></h1>
								<h1 class="red"><?php the_title(); ?></h1>
							</div>
						</div>
					</div>
				</section>
				<section class="section">
					<div class="row description-info-row">
						<div class="col-12">
							<div class="description">
								<p><?php the_field('program_description'); ?></p>
							</div>
						</div>
					</div>
				</section>
				<section class="section">
					<div class="row details-row">
						<div class="col-12">
							<div class="row">
								<?php if(have_rows('program_details')): ?>
									<?php while(have_rows('program_details')) : the_row(); ?>
										<div class="col-6 col-md-6 col-lg-4 single-detail">
										<?php if(get_sub_field('file') != null) : ?>
											<?php $file = get_sub_field('file') ?>
											<a href="<?php echo $file['url']; ?>" target="_blank">
												<div class="detail">
													<?php $image = get_sub_field('icon') ?>
													<img src="<?php echo $image['url']  ?>" alt="">
													<p class="title"><?php the_sub_field('title') ?></p>	
													<p class="sub-title"><?php the_sub_field('sub_title') ?></p>	
												</div>
											</a>

										<?php elseif(get_sub_field('link') != null) : ?>
											<a href="<?php the_sub_field('link') ?>">
												<div class="detail">
													<?php $image = get_sub_field('icon') ?>
													<img src="<?php echo $image['url']  ?>" alt="">
													<p class="title"><?php the_sub_field('title') ?></p>	
													<p class="sub-title"><?php the_sub_field('sub_title') ?></p>	
												</div>
												</a>
										<?php else: ?>
											<div class="detail">
													<?php $image = get_sub_field('icon') ?>
													<img src="<?php echo $image['url']  ?>" alt="">
													<p class="title"><?php the_sub_field('title') ?></p>	
													<p class="sub-title"><?php the_sub_field('sub_title') ?></p>	
												</div>
										<?php endif; ?>
										</div>
									<?php endwhile; ?>
								<?php endif; ?>
							</div>
						</div>
					</div>
				</section>
				<section class="section">
					<div class="row apply-now-row">
						<div class="col-12">
							<div class="apply-now">
							<a href="<?php the_field('scholarship_link') ?>"> <?php the_field('scholarship_text'); ?></a>
							</div>
						</div>
					</div>
				</section>
			</div>
		</div>
	</div>
</div>

<?php 
get_footer();