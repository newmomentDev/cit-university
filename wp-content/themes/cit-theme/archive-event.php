<?php
get_header();?>
<div class="container-fluid">
	<section class="section">
		<div class="row news-events-row">
		<?php wp_nav_menu( array( 'theme_location' => 'news-events-menu' ) ); ?>
		</div>
	</section>
	<?php
					$args = array(
								'post_type' => array('event'),
								'posts_per_page' => -1,
								'order' => 'DESC',
								'orderby' => 'post_date'
							);
					
					$loop = new WP_Query($args);	
					?>
					<?php if(!empty($loop)){ ?>
					<section class="section events-section">
						<div class="container-1600">
							<div class="col-12">
								<div class="row">
									<div class="col-md-12 title-events">
										<h1 class="black-title"><?php the_field('events_section_title', 2) ?></h1>
										<h1 class="red-title"><?php the_field('events_section_sub_title', 2) ?></h1>
									</div>
								</div>
								<div class="row">
									<?php $count = 0; ?>
									<?php while($loop->have_posts()) : $loop->the_post() ?>					
											<div class="col-md-6 col-lg-6 col-xl-4">
												<a href="<?php the_permalink(); ?>">	
													<div class="event-post">
														<?php $var = get_the_post_thumbnail_url(); ?>
														<div class="image" style="background-image: url(<?php echo $var; ?>);"></div>
														<div class="info">
															<h3 class="title"><?php the_title(); ?></h3>
															<div class="date">
															<i class="fa fa-calendar"></i><p><?php the_field('start_date') ?></p> 
															<?php if(get_field('start_date') != get_field('end_date')): ?>
															<p>- <?php the_field('end_date') ?></p>
															<?php endif; ?>
														</div>
														</div>
													</div>
												</a>
											</div>
										<?php $count++; ?>	
									<?php endwhile; ?>
								</div>
								<?php wp_reset_postdata(); } ?>
							</div>	
						</div>
					</section>
</div>

<?php
get_footer();