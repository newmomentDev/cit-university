<?php
/**
 * Template Name: Scholarship
 *
 * Template for displaying a page without sidebar even if a sidebar widget is published.
 *
 * @package understrap
 */

get_header();?>
<div class="container-fluid">
	<div class="row">
		<div class="col-12">
			<div class="form">
				<div class="form-title">
					<p class="contact-form-title"><?php the_field('form_title'); ?></p>
					<p class="form-reply-title"><?php the_field('success_title') ?></p>
				</div>
				<div class="contact-form">
				<?php $formId = get_field('scholarship_form_id');
					  $formTitle = get_field('scholarship_form_title');?>
				<?php echo do_shortcode( '[contact-form-7 id="'.$formId.'" title="'.$formTitle.'"]' );?>
				</div>

				<div class="form-reply">
				<?php if( have_rows('thank_you_message') ): 
					while( have_rows('thank_you_message') ): the_row();  ?>
					<div class="response">
						<h3 class="title"><?php the_sub_field('title')?></h3>
						<p><?php the_sub_field('description') ?> </p>
					</div>
					<div class="cta-area">
						<h2 class="cta-heading"><?php the_field('heading'); ?></h2>
						<h2 class="cta-sub-heading"><?php the_field('sub_heading'); ?></h2>
						
						<a href="<?php the_field('cta_link') ?>" class="cta-link"><?php the_field('cta_title'); ?></a>
					</div>
				<?php endwhile; ?>
				<?php endif; ?>
				</div>

			</div>
				<div class="row d-flex justify-content-center">
				<div class="col-12 col-lg-8">
					<div class="description">
						<?php the_field('description'); ?>
					</div>
				</div>
			</div>	
			
		</div>
	</div>
</div>
<script>
jQuery(function ($) { 
	$(document).on('wpcf7mailsent', function(event) {
	   	var name = $('.wpcf7 input[name=your-name]').val();
		var surname = $('.wpcf7 input[name=your-surname]').val();
		var gpa = $('.wpcf7 input[name=your-gpa]').val();
		var email = $('.wpcf7 input[name=your-email]').val();
		console.log(name + surname + email + gpa);
		$('.contact-form-title').fadeOut();
		$('.form-reply-title').fadeIn();
		$('.contact-form').animate({ height : 0, opacity: 0 });
		$('.contact-form').css('visibility', 'hidden');
		$('.form-reply .response .title').append(' ' + name + ' ' + surname )
		$('.form-reply').delay(1500).animate({height: 400, opacity: 1});
	});
});
</script>

<?php
get_footer();

