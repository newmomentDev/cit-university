<?php
/**
 * Template Name: Contact Us
 *
 * Template for displaying a page without sidebar even if a sidebar widget is published.
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

get_header();
$container = get_theme_mod( 'understrap_container_type' );
?>
<div class="container-fluid">
	<section class="section map-section">
		<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d11986.170034525045!2d19.8134083!3d41.3188151!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xf1c48c000dd8e503!2sCanadian+Institute+of+Technology!5e0!3m2!1sen!2s!4v1554977523449!5m2!1sen!2s" width="100%" height="400" frameborder="0" style="border:0" allowfullscreen></iframe>
	</section>

	<div class="row">
		<div class="col-12">
			<section class="section contact-data-page">
				<div class="section-title">
					<h2 class="title"><?php the_field('contacts_title'); ?></h2>
				</div>
			</section>
		</div>
		<div class="col-12 col-lg-6">
			<section class="section contact-data-section">
				<div class="contacts-data">
						<div class="contact">
						<p><?php the_field('contacts_info') ?></p>
						</div>
				</div>
			</section>
		</div>
		<div class="col-12 col-lg-6">
			<section class="section contact-form-section">
					<?php $formId = get_field('form_id'); ?>
						<?php $formTitle = get_field('form_title'); ?>
						<?php echo do_shortcode( '[contact-form-7 id="'.$formId.'" title="'.$formTitle.'"]' ); ?>
			</section>
		</div>
	</div>
</div>
<?php 
get_footer();