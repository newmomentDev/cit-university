<?php
/**
 * Template Name: Icons and Title
 *
 * Template for displaying a About Us.
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

get_header(); ?>

<div class="container-1600">
	<div class="row">
		<div class="col-12 ">
			<div class="page-title d-flex justify-content-center">
				<h1 class="red-title"><?php the_title(); ?></h1>
			</div>
		</div>
		<div class="col-12">
			<div class="row">
				<?php while(have_rows('data')) : the_row(); ?>
					<div class="col-6 col-lg-3">
						<a href="<?php the_sub_field('link') ?>" class="icon-link" target="_blank">
							<?php $image = get_sub_field('icon'); ?>
							<img src="<?php echo $image['url'] ?>" alt="">
							<h3><?php the_sub_field('title') ?></h3>
						</a>
					</div>
				<?php endwhile; ?>
			</div>
		</div>
	</div>
</div>


<?php get_footer();