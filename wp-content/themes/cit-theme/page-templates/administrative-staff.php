<?php
/**
 * Template Name: Administrative Staff
 *
 * Template for displaying a page without sidebar even if a sidebar widget is published.
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

get_header();
$container = get_theme_mod( 'understrap_container_type' );
?>


	<div class="container-fluid">
		<section class="section">
			<div class="row staff-menu-row">
			<?php wp_nav_menu( array( 'theme_location' => 'staff-menu' ) ); ?>
			</div>
		</section>
		<section class="section section-administrative-staff">
			<?php
			$args = array(
				'post_type' => 'administration-staff',
				'posts_per_page' => -1,
				'order' => 'ASC',
				'orderby' => 'post_date'
			);
			$query = new WP_Query( $args ); 
			// var_dump($query);
			?>	
			<div class="container-1600">
				<div class="row staff-row">
					<?php 
					// Check that we have query results.
					if ( $query->have_posts() ):
						// Start looping over the query results.
						while ( $query->have_posts() ) : $query->the_post();
							get_template_part( 'loop-templates/content', 'administrative-staff' );
						endwhile;
					endif;
					//Restore original posts
					wp_reset_postdata();
					?>
				</div>
			</div>
		</section>	
</div>

<?php get_footer();