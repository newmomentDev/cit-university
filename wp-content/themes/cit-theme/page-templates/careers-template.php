<?php
/**
 * Template Name: Careers
 *
 * Template for displaying a page without sidebar even if a sidebar widget is published.
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

get_header();
$container = get_theme_mod( 'understrap_container_type' );
?>

	<div class=container-fluid id="content">
		<div class="row">
		<div class="col-12 ">
			<div class="page-title d-flex justify-content-center">
				<h1 class="red-title"><?php the_title(); ?></h1>
			</div>
		</div>
		<?php
					$args = array(
								'post_type' => 'job-vacancy',
								'posts_per_page' => -1,
								'order' => 'DESC',
								'orderby' => 'post_date'
							);
					
					$loop = new WP_Query($args);	
					?>
					<div class="container-1200">
						<div class="row">
							<?php while($loop->have_posts()) : $loop->the_post() ?>					
							<div class="col-md-6 col-lg-4">
								<a href="<?php the_permalink(); ?>">	
									<div class="row job-vacancy">
										<div class="info col-9">
											<h2 class="title"><?php the_title(); ?></h2>
											<?php $term_list = wp_get_post_terms($post->ID, 'categories', array("fields" => "names")); ?>
											<?php foreach ($term_list as $term): ?>
												<p class="category"><?php echo $term; ?></p>
											<?php endforeach; ?>
											<time datetime="<?php echo get_the_date('c'); ?>" itemprop="datePublished"><i class="fa fa-calendar"></i><?php echo get_the_date(); ?></time>
										</div>
										<div class="arrow col-3">
											<i class="fa fa-chevron-right"></i>
										</div>
									</div>
								</a>
							</div>
							<?php endwhile; ?>
						</div>
					</div>

		</div><!-- .row end -->
	</div><!-- .container-fluid -->


<?php get_footer(); ?>
