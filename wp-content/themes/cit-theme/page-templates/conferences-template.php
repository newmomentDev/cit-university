<?php
/**
 * Template Name: Conferences
 *
 * Template for displaying a page without sidebar even if a sidebar widget is published.
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

get_header();
$container = get_theme_mod( 'understrap_container_type' );
?>

	<div class=container-fluid id="content">
		<div class="row">
		<div class="col-12 ">
			<div class="page-title d-flex justify-content-center">
				<h1 class="red-title"><?php the_title(); ?></h1>
			</div>
		</div>
		<?php
					$args = array(
								'post_type' => 'conference',
								'posts_per_page' => -1,
								'order' => 'DESC',
								'orderby' => 'post_date'
							);
					
					$loop = new WP_Query($args);	
					?>
					<div class="container-1200">
						<div class="row">
							<?php while($loop->have_posts()) : $loop->the_post() ?>					
							<div class="col-md-4 col-lg-3">
								<a href="<?php the_permalink(); ?>">	
									<div class="single-conference">
										<?php $image = get_the_post_thumbnail_url(); ?>
										<img src="<?php echo $image; ?>" alt="">
									</div>
								</a>
							</div>
							<?php endwhile; ?>
						</div>
					</div>

		</div><!-- .row end -->
	</div><!-- .container-fluid -->


<?php get_footer(); ?>
