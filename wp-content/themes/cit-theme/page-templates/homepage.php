<?php
/**
 * Template Name: Homepage
 *
 * Template for displaying a page without sidebar even if a sidebar widget is published.
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

get_header();
$container = get_theme_mod( 'understrap_container_type' );
?>

	<div class=container-fluid id="content">

		<div class="row">

				<main class="site-main" id="main" role="main">
					<?php 
					if(get_field('video_or_carousel') == 'carousel') :
					// check if the repeater field has rows of data
					if( have_rows('carousel_images') ): ?>
					<section class="section main-banner">
					<div id="carousel" class="carousel slide" data-ride="carousel">
						<div class="carousel-inner">
					 	<?php $count = 1; ?>
					    <?php  while ( have_rows('carousel_images') ) : the_row(); ?>
					    	<?php $image = get_sub_field('image') ?>
							<div class="carousel-item <?php if($count == 1 ){ echo "active"; }?>" >
								<div class="banner" style="background-image: url('<?php echo $image; ?>');">
									<div class="overlay-black"></div>
									<div class="title">
										<p><?php the_sub_field('main_header'); ?></p>
									</div>
									<div class="buttons">
											<a href="<?php the_sub_field('cta_link_1');  ?>"><?php the_sub_field('cta_title_1') ?></a>
											<a href="<?php the_sub_field('cta_link_2');  ?>"><?php the_sub_field('cta_title_2') ?></a>
											<a href="<?php the_sub_field('cta_link_3');?>"><?php the_sub_field('cta_title_3') ?></a>
									</div>
									<div class="scroll-down">
										<span class="scroll-down-span"></span>
									</div>
								</div>
								
						    </div>
						    <?php $count++; ?>
						<?php endwhile; ?>
						</div>
					</section>
					<?php endif;
					else: 
						if( have_rows('video_banner') ): ?>
					<section class="section main-banner">
					<div id="carousel" class="carousel slide" data-ride="carousel">
						<div class="carousel-inner">
					 	<?php $count = 1; ?>
					    <?php  while ( have_rows('video_banner') ) : the_row(); ?>
					    	<?php $video = get_sub_field('video') ?>
							<div class="video-banner <?php if($count == 1 ){ echo "active"; }?>" >
								<div class="banner">
									<video autoplay muted loop id="siteVideo">
										  <source src="<?php echo $video['url']; ?>" type="video/mp4">
									</video>
									<?php $image = get_sub_field('video_thumbnail') ?>
										<div class="video-thumbnail" style="background-image: url('<?php echo $image['url']; ?>');">
										</div>
									<div class="overlay-black"></div>
									<div class="title">
										<p><?php the_sub_field('main_header'); ?></p>
									</div>
									<div class="buttons">
											<a href="<?php the_sub_field('cta_link_1');  ?>"><?php the_sub_field('cta_title_1') ?></a>
											<a href="<?php the_sub_field('cta_link_2');  ?>"><?php the_sub_field('cta_title_2') ?></a>
											<a href="<?php the_sub_field('cta_link_3');?>"><?php the_sub_field('cta_title_3') ?></a>
									</div>
									<div class="scroll-down">
										<span class="scroll-down-span"></span>
									</div>
								</div>
								
						    </div>
						    <?php $count++; ?>
						<?php endwhile; ?>
						</div>
					</section>
					<?php endif;

					 endif;
					
					// check if the repeater field has rows of data
					if( have_rows('why_cit') ): ?>
					<section class="section why-cit-section">
						<div class="container-1600">
						<div class="why-cit">
							<div class="col-md-12 title-why-cit">
								<h1><?php the_field('why_cit_title') ?></h1>
							</div>
							<div class="col-md-12">
									<div class="row">
									<?php  while ( have_rows('why_cit') ) : the_row(); ?>
										<div class="col-6 col-lg-3 single-why-cit">
											<?php $icon = get_sub_field('icon'); ?>
											<div class="image">
												<img src="<?php echo $icon['url']; ?>" alt="">
											</div>
											<h3><?php the_sub_field('why_cit'); ?></h3>
										</div>
									<?php endwhile; ?>	
									</div>
								</div>
							</div>
						</div>
					</section>
					<?php endif; ?>
					<section class="section apply-now-section">
							<div class="apply-now">
								<div class="row">
									<div class="col-md-8 scholarship-text">
										<h1><?php the_field('scholarship_text')?></h1>
									</div>
									<div class="col-md-4 apply-now-button">
										<a href="<?php the_field('apply_now_link'); ?>"><?php the_field('apply_now_button') ?></a>
									</div>
								</div>	
							</div>
					</section>

					<?php
					$args = array(
								'post_type' => array('event'),
								'posts_per_page' => 3,
								'order' => 'DESC',
								'orderby' => 'post_date'
							);
					
					$loop = new WP_Query($args);	
					?>
					<?php if(!empty($loop)){ ?>
					<section class="section events-section">
						<div class="container-1600">
							<div class="col-12">
							<div class="row">
								<div class="col-md-12 title-events">
									<h1 class="black-title"><?php the_field('events_section_title') ?></h1>
									<h1 class="red-title"><?php the_field('events_section_sub_title') ?></h1>
								</div>
							</div>
							<div class="row">
								<?php $count = 0; ?>
								<?php while($loop->have_posts()) : $loop->the_post() ?>					
										<div class="col-md-6 col-lg-6 col-xl-4">
											<a href="<?php the_permalink(); ?>">	
												<div class="event-post">
													<?php $var = get_the_post_thumbnail_url(); ?>
													<div class="image" style="background-image: url(<?php echo $var; ?>);"></div>
													<div class="info">
														<h3 class="title"><?php the_title(); ?></h3>
														<?php $post_type = get_post_type(); ?>
														<?php if($post_type != 'news'): ?>
															<div class="date">
																<i class="fa fa-calendar"></i><p><?php the_field('start_date') ?></p> 
																<?php if(get_field('start_date') != get_field('end_date')): ?>
																<p>- <?php the_field('end_date') ?></p>
																<?php endif; ?>
															</div>
														<?php endif; ?>
													</div>
												</div>
											</a>
										</div>
									<?php $count++; ?>	
								<?php endwhile; ?>
							</div>
							<?php wp_reset_postdata(); } ?>
							<div class="row">
								<div class="col-12 see-more">
									<a href="<?php the_field('more_events_link') ?>"><?php the_field('more_events_text'); ?></a>		
								</div>
							</div>
							</div>
						</div>
					</section>

							<?php
					$args = array(
								'post_type' => array('news', 'job-vacancy'),
								'posts_per_page' => 3,
								'order' => 'DESC',
								'orderby' => 'post_date'
							);
					
					$loop = new WP_Query($args);	
					?>
					<?php if(!empty($loop)){ ?>
					<section class="section events-section">
						<div class="container-1600">
							<div class="col-12">
							<div class="row">
								<div class="col-md-12 title-events">
									<h1 class="black-title"><?php the_field('news_section_title') ?></h1>
									<h1 class="red-title"><?php the_field('news_section_sub_title') ?></h1>
								</div>
							</div>
							<div class="row">
								<?php $count = 0; ?>
								<?php while($loop->have_posts()) : $loop->the_post() ?>					
										<div class="col-md-6 col-lg-6 col-xl-4">
											<a href="<?php the_permalink(); ?>">	
												<div class="event-post">
													<?php $var = get_the_post_thumbnail_url(); ?>
													<div class="image" style="background-image: url(<?php echo $var; ?>);"></div>
													<div class="info">
														<h3 class="title"><?php the_title(); ?></h3>
														<?php $post_type = get_post_type(); ?>
															<div class="category">
															<?php $post_type	= str_replace(["-", "–"], ' ', $post_type); ?>
															<p><?php echo (ucwords($post_type));  ?></p>
															</div>
													</div>
												</div>
											</a>
										</div>
									<?php $count++; ?>	
								<?php endwhile; ?>
							</div>
							<?php wp_reset_postdata(); } ?>
							<div class="row">
								<div class="col-12 see-more">
									<a href="<?php the_field('more_events_link') ?>"><?php the_field('more_events_text'); ?></a>		
								</div>
							</div>
							</div>
						</div>
					</section>

					<?php 
					// check if the repeater field has rows of data
					if( have_rows('counters_box') ): ?>
					<section class="section counter-section">
						<div class="container-1600">
						<div class="counter-boxes">
							<div class="col-md-12 title-counter-boxes">
								<h1><?php the_field('counter_section_title') ?></h1>
							</div>
							<div class="col-md-12">
									<div class="row counters-row">
									<?php $num = 0; ?>
									<?php  while ( have_rows('counters_box') ) : the_row(); ?>
										<div class="col-6 col-lg-3">
											<div class="single-counter-values">
											<div class="stats">
												<p class="counter-count <?php the_sub_field('counter_title_p_1'); ?>"><?php the_sub_field('counter_stats') ?></p>
												<h3><?php the_sub_field('counter_title_p_1'); ?></h3>
											</div>
											<div class="data">
												<?php $icon = get_sub_field('counter_logo'); ?>
												<img src="<?php echo $icon['url']; ?>" alt="">
												<h3><?php the_sub_field('counter_title_p_2'); ?></h3>
											</div>
											</div>
										</div>
									<?php endwhile; ?>	
									</div>
								</div>
							</div>
						</div>
					</section>
					<?php endif; ?>

						<?php
					$args = array(
								'post_type' => 'testimonial',
								'posts_per_page' => -1,
							);
					
					$loop = new WP_Query($args);	
					?>
					<?php if(!empty($loop)){ ?>
					<section class="section testimonial-section">
						<div class="col-12">
							<div class="row">
								<div class="col-md-12 title-testimonial">
									<h1 class="black-title"><?php the_field('testimonial_section_title') ?></h1>
									<h1 class="red-title"><?php the_field('testimonial_section_sub_title') ?></h1>
								</div>
							</div>
							<div class="container">
							<div class="row">
								<div id="carousel-testimonial" class="carousel slide testimonial-carousel" data-ride="carousel">
									<ol class="carousel-indicators">
										<?php $count = 0; ?>
										<?php $active = 1; ?>
										<?php while($loop->have_posts()) : $loop->the_post() ?>
										<li data-target="#carousel-testimonial" data-slide-to="<?php echo $count; ?>" class="indicator <?php if($active == 1 ){ echo "active"; }?>">
											<div class="indicator-elem"></div>
										</li>
										<?php $count++; ?>
										<?php $active++; ?>
										<?php endwhile; ?>
									</ol>
			
									<div class="carousel-inner">
									<?php $count = 1; ?>
									<?php while($loop->have_posts()) : $loop->the_post() ?>
								    <?php $image = get_field('image') ?>
										<div class="carousel-item <?php if($count == 1 ){ echo "active"; }?>" >
											<div class="row">
												<div class="col-md-4 information-area">
													<img src="<?php echo $image['url']; ?>" alt="">
													<h3 class="full-name"><?php the_field('full_name') ?></h3>
													<h3 class="position"><?php the_field('position') ?></h3>
												</div>
												<div class="col-md-8 description-area">
													<p class="description"><?php the_field('description')?></p>
												</div>
											</div>
									</div>	
									<?php $count++; ?>
								    <?php endwhile; ?>
								</div>
								<div class="navigation">
									<a class="carousel-control-prev" href="#carousel-testimonial" role="button" data-slide="prev">
										<span class="carousel-control-prev-icon" aria-hidden="true"></span>
									   	<span class="sr-only">Previous</span>
									</a>
									<a class="carousel-control-next" href="#carousel-testimonial" role="button" data-slide="next">
										<span class="carousel-control-next-icon" aria-hidden="true"></span>
										<span class="sr-only">Next</span>
									</a>
								</div>
							</div>
							</div>
							
								
							</div>
					</section>
					<?php wp_reset_postdata(); } ?>

					
				

				</main><!-- #main -->

					</div><!-- #main -->


		</div><!-- .row end -->


<?php get_footer(); ?>
