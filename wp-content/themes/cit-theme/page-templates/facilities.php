<?php
/**
 * Template Name: Facilities
 *
 * Template for displaying a About Us.
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

get_header(); ?>

<div class="container-fluid">
	<div class="row">
		<div class="col-12 ">
			<div class="page-title d-flex justify-content-center">
				<h1 class="red-title"><?php the_title(); ?></h1>
			</div>
		</div>
		<div class="col-12">
			<?php if(have_rows('tab')): ?>
	<ul class="nav nav-tabs justify-content-center" role="tablist">
	<?php $count = 1; ?>
	<?php while(have_rows('tab')) : the_row(); ?>
		<?php $title = get_sub_field('tab_title'); ?>
		<?php $titleId = clean(strtolower(str_replace(" ","-",$title))); ?>
		<li class="nav-item">
			<a class="nav-link<?php if($count == 1){echo ' active';} ?>" id="<?php echo $titleId; ?>-tab" data-toggle="tab" href="#<?php echo $titleId; ?>" role="tab" aria-controls="<?php echo $titleId; ?>" aria-selected="false">
				<?php the_sub_field('tab_title'); ?>	
			</a>
		</li>
	<?php $count++; ?>				
	<?php endwhile; ?>
	</ul>
			<div class="tab-content">
				<?php $active = 1; ?>
				<?php while(have_rows('tab')) : the_row(); ?>
				<?php $title = get_sub_field('tab_title'); ?>
				<?php $titleId = clean(strtolower(str_replace(" ","-",$title))); ?>
				<div class="tab-pane fade<?php if($active == 1){echo ' active show';} ?>" id="<?php echo $titleId; ?>" role="tabpanel" aria-labelledby="<?php echo $titleId; ?>-tab">
					<div class="title d-flex justify-content-center">
						<h3 class="red-title d-flex justify-content-center"><?php echo $title; ?></h3>
						<?php if(!empty(get_sub_field('tab_subtitle'))): ?>
						<h3 class="black-title d-flex justify-content-center"><?php the_sub_field('tab_subtitle'); ?></h3>
						<?php endif; ?>
						<?php if(!empty(get_sub_field('tab_subtitle_2'))): ?>
						<h3 class="sub-title d-flex justify-content-center"><?php the_sub_field('tab_subtitle_2'); ?></h3>
						<?php endif; ?>
					</div>
					<div class="information">
						<?php the_sub_field('information'); ?>
					</div>
				</div>
				<?php $active++; ?>				
				<?php endwhile; ?>
			</div>
	<?php endif; ?>

		</div>
	</div>
</div>


<?php get_footer();