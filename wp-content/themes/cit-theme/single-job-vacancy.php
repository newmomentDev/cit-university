<?php get_header(); ?>
<div class="container-1400">
	<!-- <div class="row"> -->
		<section class="section mt-5">
			<div class="row">
				<div class="col-12">
					<a href="<?php the_field('careers_url','options') ?>" class="cta-link main-careers-button"><i class="fa fa-chevron-left"></i><?php the_field('careers_text','options') ?></a>
				</div>
			</div>
		</section>
		<section class="section">
			<div class="row">
				<div class="col-12 col-lg-8">
					<div class="info">
						<h2 class="title"><?php the_title(); ?></h2>
						<?php $term_list = wp_get_post_terms($post->ID, 'categories', array("fields" => "names")); ?>
							<?php foreach ($term_list as $term): ?>
							<p class="category"><?php echo $term; ?></p>
							<?php endforeach; ?>
						<time datetime="<?php echo get_the_date('c'); ?>" itemprop="datePublished"><i class="fa fa-calendar"></i><?php echo get_the_date(); ?></time>
					</div>
					<div class="content">
						<p><?php the_content(); ?></p>
					</div>
				</div>
				<div class="col-lg-4">
					<?php $toggleValue = get_field('toggle_button','options'); ?>
					<?php //var_dump($toggleValue); ?>
					<?php if( $toggleValue == true ): ?>
					<div class="widget-area">
						<h1 class="title"><?php the_field('widget_title','options'); ?></h1>
							<video width="100%" height="100%" controls>
								<?php $video = get_field('widget_video','options') ?>
 							  <source src="<?php echo $video['url']; ?>" type="video/mp4">
							</video>
						<p class="widget-description">
							<?php the_field('widget_description','options'); ?>
						</p>
					</div>
				<?php endif; ?>
				</div>
			</div>
			<div class="row">
				<div class="col-12">
					<div class="d-flex justify-content-center">
						<a href="mailto:<?php the_field('apply_now_email','options') ?>" class="cta-link"><?php the_field('apply_now_text','options'); ?></a>
					</div>
				</div>
			</div>
		</section>
		
	<!-- </div> -->
</div>

<?php get_footer();