<?php 
get_header(); ?>

<div class="container-fluid">
	<section class="section">
		<div class="row categories-row">
		<?php wp_nav_menu( array( 'theme_location' => 'news-events-menu' ) ); ?>
		</div>
	</section>
	<div class="container-1600">
		<div class="row">
			<div class="col-12">
				<section class="section">
					<div class="row top-info-row">
						<div class="col-lg-6">
							<?php $var = get_the_post_thumbnail_url(); ?>
							<div class="image" style="background-image: url('<?php echo $var; ?>');"></div>
						</div>
						<div class="col-lg-6">
							<div class="data">
								<h1 class="black"><?php the_title(); ?></h1>
								<h2 class="red"><?php the_field('excerpt'); ?></h2>
								
							</div>
						</div>
					</div>
				</section>
				<section class="section">
					<div class="row description-info-row">
						<div class="col-12">
							<div class="description">
								<p><?php the_content(); ?></p>
							</div>
						</div>
					</div>
				</section>
				<?php if(have_rows('gallery_carousel')): ?>
				<section class="section section-carousel">
					<div class="row">
						<div class="col-12">
							<div class="center">
								<p><?php the_field('gallery_section_title') ?></p>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-12">
							<div class="owl-carousel">
								<?php while (have_rows('gallery_carousel')) : the_row(); ?>
								<?php $image = get_sub_field('image'); ?>
								<a href="<?php echo $image['url']; ?>" data-fancybox="images">
									<div class="image">
										<img src="<?php echo $image['url']; ?>" alt="">
									</div>
								</a>
								<?php endwhile; ?>
							</div>
						</div>
					</div>
				</section>
			<?php endif; ?>
			<section class="section more-data">
				<div class="row">
					<div class="col-12 see-more">
						<a href="<?php the_field('more_news_link', 2) ?>"><?php the_field('more_news_text',2); ?></a>		
					</div>
				</div>
			</section>
			</div>
		</div>
	</div>
</div>


<?php 
get_footer();
