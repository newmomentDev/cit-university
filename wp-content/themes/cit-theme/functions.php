<?php
/**
 * Understrap functions and definitions
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

$understrap_includes = array(
    '/theme-settings.php',                  // Initialize theme default settings.
	'/cpt.php',                             // Include all CPT
	'/setup.php',                           // Theme setup and custom theme supports.
	'/widgets.php',                         // Register widget area.
	'/enqueue.php',                         // Enqueue scripts and styles.
	'/template-tags.php',                   // Custom template tags for this theme.
	'/pagination.php',                      // Custom pagination for this theme.
	'/hooks.php',                           // Custom hooks.
	'/extras.php',                          // Custom functions that act independently of the theme templates.
	'/customizer.php',                      // Customizer additions.
	'/custom-comments.php',                 // Custom Comments file.
	'/jetpack.php',                         // Load Jetpack compatibility file.
	'/class-wp-bootstrap-navwalker.php',    // Load custom WordPress nav walker.
	'/woocommerce.php',                     // Load WooCommerce functions.
	'/editor.php',                          // Load Editor functions.
	'/deprecated.php',                      // Load deprecated functions.
);

foreach ( $understrap_includes as $file ) {
	$filepath = locate_template( 'inc' . $file );
	if ( ! $filepath ) {
		trigger_error( sprintf( 'Error locating /inc%s for inclusion', $file ), E_USER_ERROR );
	}
	require_once $filepath;
}

add_filter('acf/settings/save_json', 'my_acf_json_save_point');
 
function my_acf_json_save_point( $path ) {
    
    // update path
    $path = get_stylesheet_directory() . '/acf-json';
    
    
    // return
    return $path;
    
}

add_filter('acf/settings/load_json', 'my_acf_json_load_point');

function my_acf_json_load_point( $paths ) {
    
    // remove original path (optional)
    unset($paths[0]);
    
    
    // append path
    $paths[] = get_stylesheet_directory() . '/acf-json';
    
    
    // return
    return $paths;
    
}

if( function_exists('acf_add_options_page') ) {
	
	acf_add_options_page(array(
		'page_title' 	=> 'Theme General Settings',
		'menu_title'	=> 'Theme Settings',
		'menu_slug' 	=> 'theme-general-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));
	
	acf_add_options_sub_page(array(
		'page_title' 	=> 'Theme Header Settings',
		'menu_title'	=> 'Header',
		'parent_slug'	=> 'theme-general-settings',
	));
	
	acf_add_options_sub_page(array(
		'page_title' 	=> 'Theme Footer Settings',
		'menu_title'	=> 'Footer',
		'parent_slug'	=> 'theme-general-settings',
	));
	
}

/**
 * Enqueue a script with jQuery as a dependency.
 */
function wpdocs_scripts_method() {
    wp_enqueue_script( 'custom-script', get_stylesheet_directory_uri() . '/js/app.js', '1.3.5' , array( 'jquery' ), true , 11);
}
add_action( 'wp_enqueue_scripts', 'wpdocs_scripts_method', 20, 1 );



function footer_menu_widget() {
    register_sidebar( array(
        'name' => __( 'Footer Menu First', 'footermenu' ),
        'id' => 'footer-menu-first',
        'before_widget' => '<div class="footer-menu menu-first">',
        'after_widget' => '</div>',
        'before_title' => '<h4>',
        'after_title' => '</h4>',
    ) );
}
add_action( 'widgets_init', 'footer_menu_widget' );

function footer_menu_widget_2() {
    register_sidebar( array(
        'name' => __( 'Footer Menu Second', 'footermenu' ),
        'id' => 'footer-menu-second',
        'before_widget' => '<div class="footer-menu menu-second">',
        'after_widget' => '</div>',
        'before_title' => '<h4>',
        'after_title' => '</h4>',
    ) );
}
add_action( 'widgets_init', 'footer_menu_widget_2' );


function register_faculties_menu() {
  register_nav_menu('faculties-menu',__( 'Faculties Menu' ));
}
add_action( 'init', 'register_faculties_menu' );

function register_staff_menu() {
  register_nav_menu('staff-menu',__( 'Staff Menu' ));
}
add_action( 'init', 'register_staff_menu' );

function register_footer_first_menu() {
  register_nav_menu('footer-menu-first',__( 'First Footer Menu' ));
}
add_action( 'init', 'register_footer_first_menu' );

function register_footer_second_menu() {
  register_nav_menu('footer-menu-second',__( 'Second Footer Menu' ));
}
add_action( 'init', 'register_footer_second_menu' );

function register_news_events_menu() {
  register_nav_menu('news-events-menu',__( 'News & Events Menu' ));
}
add_action( 'init', 'register_news_events_menu' );

//Adding custom script 
function wpdocs_theme_name_scripts() {
    wp_enqueue_script( 'scroll-reveal', get_template_directory_uri() . '/js/scrollreveal.min.js');
    wp_enqueue_style('owl-carousel-default', get_template_directory_uri() . '/css/owl.theme.default.min.css');
    wp_enqueue_style('owl-carousel-css', get_template_directory_uri() . '/css/owl.carousel.min.css');
    wp_enqueue_script( 'owl-carousel-js', get_template_directory_uri() . '/js/owl.carousel.min.js');
				wp_enqueue_style('fancybox-css', get_template_directory_uri() . '/css/jquery.fancybox.min.css');
    wp_enqueue_script( 'fancybox-js', get_template_directory_uri() . '/js/jquery.fancybox.min.js');

}
add_action( 'wp_enqueue_scripts', 'wpdocs_theme_name_scripts' );

//CF7 remove P tags
define( 'WPCF7_AUTOP', false );

// Remove subtitles from WPML
add_filter( 'wpml_custom_field_original_data', 'disable_original_lang_data' );

function disable_original_lang_data(){
return;
}

//Cleaning strings
function clean($string) {
	   $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.

	   return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
	}


//Removing exerpt
function change_excerpt( $text )
{
    $pos = strrpos( $text, '[');
    if ($pos === false)
    {
        return $text;
    }

    return rtrim (substr($text, 0, $pos) );
}
add_filter('get_the_excerpt', 'change_excerpt');

//Adding Google Api Key
function my_acf_init() {
	
	acf_update_setting('google_api_key', 'ABQIAAAAvZMU4-DFRYtw1UlTj_zc6hT2yXp_ZAY8_ufC3CFXhHIE1NvwkxQcT1h-VA8wQL5JBdsM5JWeJpukvw');
}

add_action('acf/init', 'my_acf_init');


function monoprog_get_menu_by_location($location) {
    if(empty($location)) return false;

    $locations = get_nav_menu_locations();
    if(!isset($locations[$location])) return false;

    return get_term( $locations[$location], 'nav_menu' );
}