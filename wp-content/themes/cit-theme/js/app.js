      jQuery(document).ready(function($) {
            //$ is now jQuery 
            $('.scroll-down-span').click(function() {
                  $('html, body').animate({ scrollTop: $('section.why-cit-section').offset().top - 100 }, 'slow');
                  return false;
            });

            if (!$('body').hasClass("page-template-homepage")) {
                  $('.bg-primary').addClass('bg-different');
            }

            $(window).scroll(function() {
                  if ($(window).scrollTop() >= 300) {
                        $('nav').addClass('scrolled');
                  } else {
                        $('nav').removeClass('scrolled');
                  }
            });

            function scrollCount() {
                  $('.counter-count').each(function() {
                        $(this).prop('Counter', 0).animate({
                              Counter: $(this).text()
                        }, {
                              duration: 2500,
                              easing: 'swing',
                              step: function(now) {
                                    $(this).text(Math.ceil(now));
                              }
                        });
                  }); //reached the desired point -- show div         
            }

            function prepend() {
                  $('p.counter-count.students').prepend('+').fadeIn('slow');
                  $('p.counter-count.Proffesors').prepend('+').fadeIn('slow');
                  $('p.counter-count.ratio').prepend('1/').fadeIn('slow');
                  $('p.counter-count.studentë').prepend('+').fadeIn('slow');
                  $('p.counter-count.profesorë').prepend('+').fadeIn('slow');
                  $('p.counter-count.raporti').prepend('1/').fadeIn('slow');
            }

            ScrollReveal().reveal('.counter-section', {
                  duration: 500,
                  viewOffset: {
                        top: 60
                  },
                  afterReveal: function(el) {
                        scrollCount();

                        setTimeout(function() { prepend(); }, 3000);
                  }
            });


            var btn = $('.to-top');

            $(window).scroll(function() {
                  if ($(window).scrollTop() > 350) {
                        btn.fadeIn(450);
                  } else {
                        btn.fadeOut(450);
                  }
            });

            btn.on('click', function(e) {
                  e.preventDefault();
                  $('html, body').animate({ scrollTop: 0 }, '700');
            });


            $(".side-strip .side-strip-list li").each(function() {
                  $(this).click(function() {
                        event.preventDefault();
                        $(this).next("div").toggleClass('show');
                        $(this).find('i').toggleClass('custom-icon');
                        $(this).siblings('li').toggleClass('hide');
                        $(this).toggleClass('active');

                  });
            });

            if ($('body').hasClass('news-template-default')) {
                  $('#menu-news-events-menu li:last-child').addClass('current-menu-parent');
            }

            if ($('body').hasClass('event-template-default')) {
                  $('#menu-news-events-menu li:first-child').addClass('current-menu-parent');
            }

            $().fancybox({
                  selector: 'owl-item:not(.cloned) a',
                  hash: false,
                  thumbs: {
                        autoStart: true
                  },
                  buttons: [
                        'zoom',
                        'download',
                        'close'
                  ]
            });

            $('.owl-carousel').owlCarousel({
                  loop: false,
                  margin: 10,
                  height: 250

            });


            $(".nav-tabs a.nav-link").on('click', function() {
                  $(".tab-pane").hide();
                  $($(this).attr("href")).show();
            });


            $(".error404 .cta-link").on('click', function() {
                  window.history.back();
            });

            $('.wpcf7-list-item input:radio').on('click', function() {
                  $(this).parent().addClass('active');
                  $(this).parent().siblings().removeClass('active');
            });

             $('.video-section .close').on('click', function() {
                  $('video').remove();
            });

      });