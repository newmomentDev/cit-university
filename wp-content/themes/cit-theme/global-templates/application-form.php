<!-- Modal -->
<!-- modal fade -->
<div class="modal fade" id="application-form" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"><?php the_field('modal_title') ?></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
							<?php $formId = get_field('form_id'); ?>
            <?php $formTitle = get_field('form_title'); ?>
            <?php echo do_shortcode( '[contact-form-7 id="'.$formId.'" title="'.$formTitle.'"]' ); ?>
      </div>
    </div>
  </div>
</div>