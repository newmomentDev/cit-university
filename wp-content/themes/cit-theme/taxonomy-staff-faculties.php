<?php
get_header();?>
<div class="container-fluid">
	
	<div class="row staff-menu-row">
	<?php wp_nav_menu( array( 'theme_location' => 'staff-menu' ) ); ?>
	</div>

	<?php
			global $wp;
			$cat = explode("/", $wp->request);

			function return_args ( $terms, $meta_query = array()){
				return array(
				'post_type' => 'academic-staff',
				'posts_per_page' => -1,
				'order' => 'ASC',
				'orderby' => 'post_date',
				'tax_query' => array(
					'relation' => 'AND',
					array(
						'taxonomy' => 'staff-faculties',
						'field'    => 'slug',
						'terms'    => $terms,
						),
					),
				'meta_query'	=> array(
					'relation'		=> 'AND',
						array(
							'key'	 	=> 'position',
							'value'	  	=> $meta_query,
							'compare' 	=> 'IN',
						)
					)
			);
			}
			$my_args = return_args($cat[1],array('rector','vice rector'));
			$query = new WP_Query( $my_args ); ?>	
			
			<div class="container-fluid">
				<section class="section staff-section">
					<div class="row staff-row rectors-row">
									<?php 
								// Check that we have query results.
								if ( $query->have_posts() ) {
								    // Start looping over the query results.
								    while ( $query->have_posts() ) { $query->the_post();?>

								    	<?php get_template_part( 'loop-templates/content', 'persona' ); ?>
								     
								    <?php }
								}
								// Restore original p
								wp_reset_postdata();
								?>
					</div>
					<?php
					$my_args = return_args($cat[1],array('dean'));
					$query = new WP_Query( $my_args ); ?>

					<div class="row staff-row odd-row dean-row">
									<?php 
								// Check that we have query results.
								if ( $query->have_posts() ) {
								    // Start looping over the query results.
								    while ( $query->have_posts() ) { $query->the_post();?>

								    	<?php get_template_part( 'loop-templates/content', 'persona' ); ?>
								     
								    <?php }
								}
								// Restore original p
								wp_reset_postdata();
								?>
					</div>

					<?php
					$my_args = return_args($cat[1],array('head of department'));
					$query = new WP_Query( $my_args ); ?>

					<div class="row staff-row departatment-head-row">
									<?php 
								// Check that we have query results.
								if ( $query->have_posts() ) {
								    // Start looping over the query results.
								    while ( $query->have_posts() ) { $query->the_post();?>

								    	<?php get_template_part( 'loop-templates/content', 'persona' ); ?>
								     
								    <?php }
								}
								// Restore original p
								wp_reset_postdata();
								?>
					</div>
					<?php
					$my_args = return_args($cat[1], array('lecturer'));
					$query = new WP_Query( $my_args ); ?>

					<div class="row staff-row odd-row lecturer-row">
									<?php 
								// Check that we have query results.
								if ( $query->have_posts() ) {
								    // Start looping over the query results.
								    while ( $query->have_posts() ) { $query->the_post();?>

								    		<?php get_template_part( 'loop-templates/content', 'persona-lecturer' ); ?>
								     
								    <?php }
								}
								// Restore original p
								wp_reset_postdata();
								?>
					</div>

				</div>
			</section>
				

			
</div>
<?php get_footer();